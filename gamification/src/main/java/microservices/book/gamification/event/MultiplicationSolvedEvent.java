package microservices.book.gamification.event;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Event received when a multiplication has been solved in the system.
 * Provides some context information about the multiplication.
 */

class MultiplicationSolvedEvent implements Serializable {

    private Long multiplicationResultAttemptId;
    private Long userId;
    private boolean correct;

    public MultiplicationSolvedEvent() {
    }

    public MultiplicationSolvedEvent(Long multiplicationResultAttemptId, Long userId, boolean correct) {
        this.multiplicationResultAttemptId = multiplicationResultAttemptId;
        this.userId = userId;
        this.correct = correct;
    }

    public Long getMultiplicationResultAttemptId() {
        return multiplicationResultAttemptId;
    }

    public Long getUserId() {
        return userId;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setMultiplicationResultAttemptId(Long multiplicationResultAttemptId) {
        this.multiplicationResultAttemptId = multiplicationResultAttemptId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MultiplicationSolvedEvent that = (MultiplicationSolvedEvent) o;

        return new EqualsBuilder().append(correct, that.correct)
                                  .append(multiplicationResultAttemptId, that.multiplicationResultAttemptId)
                                  .append(userId, that.userId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(multiplicationResultAttemptId).append(userId).append(correct)
                                          .toHashCode();
    }
}
